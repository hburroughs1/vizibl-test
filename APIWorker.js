let dataArray = [];
let rowCount = 0;

function fetchAPI() {
    fetch('https://ergast.com/api/f1/constructors.json?limit=250')
        .then((response) => response.json())
        .then((data) => {
            dataArray = data.MRData.ConstructorTable.Constructors
            fillRows(10)
        });
}

function fillRows(maxRows) {
    maxRows = maxRows + rowCount;
    let dataTable = document.getElementById('DataTable').getElementsByTagName('tbody')[0];
    for (let i = rowCount; i < maxRows; i++) {
        let newRow = dataTable.insertRow();
        newRow.innerHTML = "<tr> <td>" + dataArray[i].name + "</td>"
            + "<td>" + dataArray[i].nationality + "</td>"
            + "<td> <a href='" + dataArray[i].url + "'>Link</a></td></tr>";
        rowCount++;
    }
}