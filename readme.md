# API

This project pulls basic team data from the [ergast.com Formula 1 developer API](http://ergast.com/mrd/). All constructors that have competed in F1 history are listed alphabetically.

## Usage

Load index.html in a modern web browser of choice. There are no external dependencies, but the css and js files should be present in the same folder as the html file for the project to work.

## Licenses
None!